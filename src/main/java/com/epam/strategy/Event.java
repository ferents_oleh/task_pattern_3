package com.epam.strategy;

import com.epam.enums.EventType;

public interface Event {
    EventType getEventType();
}
