package com.epam.strategy.impl;

import com.epam.enums.EventType;
import com.epam.strategy.Event;

public class Funeral implements Event {
    @Override
    public EventType getEventType() {
        return EventType.Funeral;
    }
}
