package com.epam.decorator.impl;

import com.epam.decorator.BouquetDecorator;

public class Packaging extends BouquetDecorator {
    private final double PACKAGING_PRICE = 10;

    public Packaging() {
        setAdditionalPrice(PACKAGING_PRICE);
    }
}
