package com.epam.decorator.impl;

import com.epam.decorator.BouquetDecorator;
import com.epam.model.Bouquet;
import com.epam.model.Flower;

public class Orchid extends BouquetDecorator {
    private final double ADDITIONAL_PRICE = 32;
    private final Flower ADDITIONAL_FLOWER = new Flower("Orchids", "red", ADDITIONAL_PRICE);

    public Orchid(Bouquet bouquet) {
        setAdditionalFlower(ADDITIONAL_FLOWER);
        setAdditionalPrice(ADDITIONAL_PRICE);
        setBouquet(bouquet);
    }
}
