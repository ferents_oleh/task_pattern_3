package com.epam.decorator.impl;

import com.epam.decorator.BouquetDecorator;
import com.epam.model.Bouquet;
import com.epam.model.Flower;

public class Tulip extends BouquetDecorator {
    private final double ADDITIONAL_PRICE = 20;
    private final Flower ADDITIONAL_FLOWER = new Flower("Tulip", "orange", ADDITIONAL_PRICE);

    public Tulip(Bouquet bouquet) {
        setAdditionalFlower(ADDITIONAL_FLOWER);
        setAdditionalPrice(ADDITIONAL_PRICE);
        setBouquet(bouquet);
    }
}
