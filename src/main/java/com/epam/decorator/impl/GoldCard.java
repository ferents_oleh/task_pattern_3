package com.epam.decorator.impl;

import com.epam.decorator.BouquetDecorator;

public class GoldCard extends BouquetDecorator {
    private final double GOLD_CARD_DELIVERY_PRICE = 0;

    public GoldCard() {
        setAdditionalPrice(GOLD_CARD_DELIVERY_PRICE);
    }
}
