package com.epam.decorator.impl;

import com.epam.decorator.BouquetDecorator;
import com.epam.model.Bouquet;
import com.epam.model.Flower;

public class Eucalyptus extends BouquetDecorator {
    private final double ADDITIONAL_PRICE = 16;
    private final Flower ADDITIONAL_FLOWER = new Flower("Eucalyptus", "green", ADDITIONAL_PRICE);

    public Eucalyptus(Bouquet bouquet) {
        setAdditionalFlower(ADDITIONAL_FLOWER);
        setAdditionalPrice(ADDITIONAL_PRICE);
        setBouquet(bouquet);
    }
}
