package com.epam.decorator.impl;

import com.epam.decorator.BouquetDecorator;
import com.epam.model.Bouquet;
import com.epam.model.Flower;

public class EustomaWhite extends BouquetDecorator {
    private final double ADDITIONAL_PRICE = 40;
    private final Flower ADDITIONAL_FLOWER = new Flower("Eustoma", "white", ADDITIONAL_PRICE);

    public EustomaWhite(Bouquet bouquet) {
        setAdditionalFlower(ADDITIONAL_FLOWER);
        setAdditionalPrice(ADDITIONAL_PRICE);
    }
}
