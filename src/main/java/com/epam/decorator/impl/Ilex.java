package com.epam.decorator.impl;

import com.epam.decorator.BouquetDecorator;
import com.epam.model.Bouquet;
import com.epam.model.Flower;

public class Ilex extends BouquetDecorator {
    private final double ADDITIONAL_PRICE = 25;
    private final Flower ADDITIONAL_FLOWER = new Flower("Ilex", "yellow", ADDITIONAL_PRICE);

    public Ilex(Bouquet bouquet) {
        setAdditionalFlower(ADDITIONAL_FLOWER);
        setAdditionalPrice(ADDITIONAL_PRICE);
        setBouquet(bouquet);
    }
}
