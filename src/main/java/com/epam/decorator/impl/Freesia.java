package com.epam.decorator.impl;

import com.epam.decorator.BouquetDecorator;
import com.epam.model.Bouquet;
import com.epam.model.Flower;

public class Freesia extends BouquetDecorator {
    private final double ADDITIONAL_PRICE = 17;
    private final Flower ADDITIONAL_FLOWER = new Flower("Freesia", "white", ADDITIONAL_PRICE);

    public Freesia(Bouquet bouquet) {
        setAdditionalFlower(ADDITIONAL_FLOWER);
        setAdditionalPrice(ADDITIONAL_PRICE);
        setBouquet(bouquet);
    }
}
