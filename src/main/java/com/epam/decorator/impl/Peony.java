package com.epam.decorator.impl;

import com.epam.decorator.BouquetDecorator;
import com.epam.model.Bouquet;
import com.epam.model.Flower;

public class Peony extends BouquetDecorator {
    private final double ADDITIONAL_PRICE = 27;
    private final Flower ADDITIONAL_FLOWER = new Flower("Peony", "white", ADDITIONAL_PRICE);

    public Peony(Bouquet bouquet) {
        setAdditionalFlower(ADDITIONAL_FLOWER);
        setAdditionalPrice(ADDITIONAL_PRICE);
        setBouquet(bouquet);
    }
}
