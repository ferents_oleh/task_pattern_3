package com.epam.decorator.impl;

import com.epam.decorator.BouquetDecorator;

public class Delivery extends BouquetDecorator {
    private final double DELIVERY_PRICE = 100;

    public Delivery() {
        setAdditionalPrice(DELIVERY_PRICE);
    }
}
