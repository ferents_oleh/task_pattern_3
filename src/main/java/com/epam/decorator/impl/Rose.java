package com.epam.decorator.impl;

import com.epam.decorator.BouquetDecorator;
import com.epam.model.Bouquet;
import com.epam.model.Flower;
import com.epam.model.impl.CustomBouquet;

public class Rose extends BouquetDecorator {
    private final double ADDITIONAL_PRICE = 35;
    private final Flower ADDITIONAL_FLOWER = new Flower("Rose", "red", ADDITIONAL_PRICE);

    public Rose(Bouquet bouquet) {
        setAdditionalFlower(ADDITIONAL_FLOWER);
        setAdditionalPrice(ADDITIONAL_PRICE);
        setBouquet(bouquet);
    }
}
