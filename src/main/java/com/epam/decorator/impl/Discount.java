package com.epam.decorator.impl;

import com.epam.decorator.BouquetDecorator;

public class Discount extends BouquetDecorator {
    private final double DISCOUNT = -50;

    public Discount() {
        setAdditionalPrice(DISCOUNT);
    }
}
