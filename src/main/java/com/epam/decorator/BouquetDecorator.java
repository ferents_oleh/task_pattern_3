package com.epam.decorator;

import com.epam.model.Bouquet;
import com.epam.model.Flower;

import java.util.List;

public class BouquetDecorator implements Bouquet{
    private Bouquet bouquet;

    private double additionalPrice;

    private Flower additionalFlower;

    public void setBouquet(Bouquet bouquet) {
        this.bouquet = bouquet;
        if (additionalFlower != null) {
            bouquet.getFlowers().add(additionalFlower);
        }
    }

    protected void setAdditionalPrice(double additionalPrice) {
        this.additionalPrice = additionalPrice;
    }

    public void setAdditionalFlower(Flower additionalFlower) {
        this.additionalFlower = additionalFlower;
    }

    @Override
    public String getName() {
        return bouquet.getName();
    }

    @Override
    public double getPrice() {
        return bouquet.getPrice() + additionalPrice;
    }

    @Override
    public List<Flower> getFlowers() {
        return bouquet.getFlowers();
    }
}
