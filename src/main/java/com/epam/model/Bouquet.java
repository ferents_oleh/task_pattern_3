package com.epam.model;

import java.util.List;

public interface Bouquet {
    String getName();

    double getPrice();

    List<Flower> getFlowers();
}
