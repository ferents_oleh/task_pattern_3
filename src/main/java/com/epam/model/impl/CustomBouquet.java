package com.epam.model.impl;

import com.epam.model.Bouquet;
import com.epam.model.Flower;

import java.util.ArrayList;
import java.util.List;

public class CustomBouquet implements Bouquet {

    private final String NAME = "Custom Bouquet";
    private List<Flower> flowers = new ArrayList<>();

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public double getPrice() {
        double totalPrice = 0;
        for (Flower flower : flowers) {
            totalPrice = totalPrice + flower.getPrice();
        }
        return totalPrice;
    }

    @Override
    public List<Flower> getFlowers() {
        return flowers;
    }
}
