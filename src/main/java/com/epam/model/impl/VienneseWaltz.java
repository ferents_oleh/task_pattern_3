package com.epam.model.impl;

import com.epam.model.Bouquet;
import com.epam.model.Flower;

import java.util.ArrayList;
import java.util.List;

public class VienneseWaltz implements Bouquet {
    private final String NAME = "Viennese Waltz";

    private final double PRICE = 540;

    private List<Flower> flowers;

    public VienneseWaltz() {
        flowers = new ArrayList<>();
        flowers.add(new Flower("Eustoma", "Red"));
        flowers.add(new Flower("Eustoma", "White"));
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public double getPrice() {
        return PRICE;
    }

    @Override
    public List<Flower> getFlowers() {
        return flowers;
    }
}
