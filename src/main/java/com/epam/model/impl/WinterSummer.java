package com.epam.model.impl;

import com.epam.model.Bouquet;
import com.epam.model.Flower;

import java.util.ArrayList;
import java.util.List;

public class WinterSummer implements Bouquet {
    private final String NAME = "Rose flame";

    private final double PRICE = 660;

    private List<Flower> flowers;

    public WinterSummer() {
        flowers = new ArrayList<>();
        flowers.add(new Flower("Rose", "Red"));
        flowers.add(new Flower("Larkspur", "White"));
        flowers.add(new Flower("Peony", "Red"));
        flowers.add(new Flower("Tulip", "White"));
        flowers.add(new Flower("Eucalyptus", "Green"));
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public double getPrice() {
        return PRICE;
    }

    @Override
    public List<Flower> getFlowers() {
        return flowers;
    }
}
