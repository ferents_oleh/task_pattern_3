package com.epam.model.impl;

import com.epam.model.Bouquet;
import com.epam.model.Flower;

import java.util.ArrayList;
import java.util.List;

public class RoseFlameBouquet implements Bouquet {
    private final String NAME = "Rose flame";

    private final double PRICE = 600;

    private List<Flower> flowers;

    public RoseFlameBouquet() {
        flowers = new ArrayList<>();
        flowers.add(new Flower("Rose", "Red"));
        flowers.add(new Flower("Ilex", "Yellow"));
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public double getPrice() {
        return PRICE;
    }

    @Override
    public List<Flower> getFlowers() {
        return flowers;
    }
}
