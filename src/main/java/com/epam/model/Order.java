package com.epam.model;

import com.epam.strategy.Event;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Order {
    private Bouquet bouquet;

    private Event event;

    private Customer customer;
}
