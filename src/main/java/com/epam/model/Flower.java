package com.epam.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Flower {

    public Flower(String name, String color) {
        this.name = name;
        this.color = color;
    }

    private String name;

    private String color;

    private double price;
}
