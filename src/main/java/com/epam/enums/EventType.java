package com.epam.enums;

public enum EventType {
    Wedding, Birthday, Funeral, ValentineDay
}
