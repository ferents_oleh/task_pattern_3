package com.epam;

import com.epam.controller.impl.OrderBouquetControllerImpl;
import com.epam.view.Menu;

public class Application {

    public static void main(String[] args) {
        new Menu(new OrderBouquetControllerImpl());
    }
}
