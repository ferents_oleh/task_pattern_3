package com.epam.controller.impl;

import com.epam.controller.OrderBouquetController;
import com.epam.decorator.BouquetDecorator;
import com.epam.decorator.impl.*;
import com.epam.model.Bouquet;
import com.epam.model.Customer;
import com.epam.model.Order;
import com.epam.model.impl.CustomBouquet;
import com.epam.model.impl.RoseFlameBouquet;
import com.epam.model.impl.VienneseWaltz;
import com.epam.model.impl.WinterSummer;
import com.epam.strategy.impl.Birthday;
import com.epam.strategy.impl.Funeral;
import com.epam.strategy.impl.ValentineDay;
import com.epam.strategy.impl.Wedding;
import lombok.extern.log4j.Log4j2;

import java.util.Scanner;

@Log4j2
public class OrderBouquetControllerImpl implements OrderBouquetController {
    private Bouquet bouquet;

    private Order order = new Order();

    private BouquetDecorator decorator = new BouquetDecorator();

    @Override
    public void orderCatalogBouquet() {
        Scanner scanner = new Scanner(System.in);

        log.info("Choose a bouquet: ");
        log.info("1. Rose flame bouquet");
        log.info("2. Viennese waltz");
        log.info("3. Winter-Summer");
        int bouquetId = scanner.nextInt();
        chooseBouquet(bouquetId);

        log.info("Choose event type: ");
        log.info("1. Birthday");
        log.info("2. Funeral");
        log.info("3. Wedding");
        log.info("4. Valentine's Day");

        int eventId = scanner.nextInt();
        chooseEvent(eventId);

        log.info("Enter your personal information:");
        log.info("Enter first name: ");
        String firstName = scanner.next();
        log.info("Enter second name: ");
        String lastName = scanner.next();
        log.info("Enter a phone number: ");
        int phoneNumber = scanner.nextInt();
        order.setCustomer(new Customer(firstName, lastName, phoneNumber));

        log.info("Order for " +
                order.getCustomer().getFirstName() + " " + order.getCustomer().getLastName()
                + " is registered:");
        log.info("Bouquet: " + order.getBouquet().getName()
                + " for " + order.getEvent().getEventType().name().toLowerCase());
        log.info("Price: " + order.getBouquet().getPrice());
    }

    private void chooseBouquet(int bouquetId) {
        switch (bouquetId) {
            case 1:
                bouquet = new RoseFlameBouquet();
                order.setBouquet(bouquet);
                break;
            case 2:
                bouquet = new VienneseWaltz();
                order.setBouquet(bouquet);
                break;
            case 3:
                bouquet = new WinterSummer();
                order.setBouquet(bouquet);
                break;
            default:
                log.info("Wrong bouquet id");
        }
    }

    private void chooseEvent(int eventId) {
        switch (eventId) {
            case 1:
                order.setEvent(new Birthday());
                break;
            case 2:
                order.setEvent(new Funeral());
                break;
            case 3:
                order.setEvent(new Wedding());
                break;
            case 4:
                order.setEvent(new ValentineDay());
                break;
            default:
                log.info("Wrong event id");
        }
    }

    @Override
    public void orderCustomBouquet() {

        Scanner sc = new Scanner(System.in);

        bouquet = new CustomBouquet();

        while (true) {
            // Choosing of Flower type
            log.info("Choose flowers would you like or choose 0 for exit: ");
            log.info("1. Rose");
            log.info("2. Tulip");
            log.info("3. Orchid");
            log.info("4. Peony");
            log.info("5. Larkspur");
            log.info("6. Freesia");
            log.info("7. Eucalyptus");
            log.info("8. Eustoma red");
            log.info("9. Eustoma white");
            log.info("0. Exit");

            int flowerId = sc.nextInt();
            if (flowerId == 0) { // Exit
                break;
            }

            // Choosing of Flowers count
            log.info("Enter the desired number of flowers: ");
            int count = sc.nextInt();
            for (int i = 0; i < count; i++) {
                // Calls method for adding new flower
                addFlower(flowerId, bouquet);
                // Calls method for setting new flower to bouquet
                decorator.setBouquet(bouquet);
            }
        }
        double price = bouquet.getPrice();

        // Calls method for getting of additional services
        double serviceCost = addAdditionalServices();
        // Counts of total price value
        double totalPrice = bouquet.getPrice() + serviceCost;
        // Prints info about ordered Flowers
        log.info("Your bouquet include " + bouquet.getFlowers().size() + " flowers");
        bouquet.getFlowers().forEach(flower -> {
            log.info("Flower Name - " + flower.getName() + " prise " + flower.getPrice() + " for one");
        });
        // Calls method discount method
        double discount = countDiscount();

        log.info("Total price: " + (totalPrice - totalPrice * discount));
        messageNews();

    }

    private void addFlower(int flowerId, Bouquet bouquet) {
        Scanner scanner = new Scanner(System.in);
        switch (flowerId) {
            case 1:
                new Rose(bouquet);
                break;
            case 2:
                new Tulip(bouquet);
                break;
            case 3:
                new Orchid(bouquet);
                break;
            case 4:
                new Peony(bouquet);
                break;
            case 5:
                new Larkspur(bouquet);
                break;
            case 6:
                new Ilex(bouquet);
                break;
            case 7:
                new Freesia(bouquet);
                break;
            case 8:
                new EustomaWhite(bouquet);
                break;
            case 9:
                new EustomaRed(bouquet);
                break;
            default:
                log.info("Wrong flower id");
        }
    }

    private double addAdditionalServices() {
        Scanner sc = new Scanner(System.in);
        double cost = 0;
        boolean exit = false;

        while (!exit) {
            log.info("Choose additional service or 0 for exit: ");
            log.info("");
            log.info("1. Targeted delivery");
            log.info("2. Delivery to the nearest store");
            log.info("3. Pack in a paper");
            log.info("4. Pack in a basket");
            log.info("0. Exit");
            // Gets User's answer
            int answer = sc.nextInt();
            // Exit case
            switch (answer) {
                case 0:
                    exit = true;
                    break;
                case 1:
                    cost = cost + deliveryToAddress();
                    log.info("You chose delivery with price: " + deliveryToAddress());
                    break;
                case 2:
                    cost = cost + deliveryToNearestStore();
                    log.info("You chose cover with price: " + deliveryToNearestStore());
                    break;
                case 3:
                    cost = cost + packToPaper();
                    log.info("You chose cover with price: " + packToPaper());
                    break;
                case 4:
                    cost = cost + packToBasket();
                    log.info("You chose cover with price: " + packToBasket());
                    break;
                default:
                    log.info("Enter correct value");
            }
        }
        return cost;
    }

    private double deliveryToAddress() {
        return 150;
    }

    private double deliveryToNearestStore() {
        return 70;
    }

    private double packToPaper() {
        return 15;
    }

    private double packToBasket() {
        return 120;
    }

    private void messageNews() {
        log.info("Waiting for you next time! Have a nice day!");
    }

    private double countDiscount() {
        double discount = 0;
        Scanner scanner = new Scanner(System.in);
        log.info("Choose discount program: ");
        log.info("1. Gold cart (20%) ");
        log.info("2. Silver cart (10%) ");

        int answer = scanner.nextInt();

        switch (answer) {
            case 1:
                discount = 0.2;
                break;
            case 2:
                discount = 0.1;
                break;
            default:
                log.info("Choose correct answer");
        }
        return discount;
    }

}
