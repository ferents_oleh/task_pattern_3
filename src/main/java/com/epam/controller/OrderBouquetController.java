package com.epam.controller;

public interface OrderBouquetController {
    void orderCatalogBouquet();

    void orderCustomBouquet();
}
